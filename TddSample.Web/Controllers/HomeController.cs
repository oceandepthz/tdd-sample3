﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TddSample.Web.Models;
using TddSample.Web.Models.Rentals;

namespace TddSample.Web.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            var customer = GetCurrentCustomer();
            return View(new EstimateViewModel(Movies, customer));
        }

        [HttpPost]
        public IActionResult Index([FromForm] string selectedMovieId, [FromForm] int daysRented)
        {
            var customer = GetCurrentCustomer();
            var movie = FindMovie(selectedMovieId);
            customer.AddRental(new Rental(movie, daysRented));

            return View(new EstimateViewModel(Movies, customer));
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        #region Customers

        private static readonly Customer CurrentCustomer = new Customer("ばくはつ 五郎");

        private static Customer GetCurrentCustomer()
        {
            return CurrentCustomer;
        }

        #endregion

        #region Movies

        private static readonly IList<Movie> Movies = new[]
        {
            new Movie("スターウォーズ IV", MovieRentalType.Regular),
            new Movie("ハン・ソロ", MovieRentalType.NewRelease),
            new Movie("親指スターウォーズ", MovieRentalType.Kids),
        }.ToImmutableArray();

        private static Movie FindMovie(string id)
        {
            return Movies.FirstOrDefault(movie => movie.Id == id);
        }

        #endregion
    }
}